FROM httpd:latest
LABEL seongwon "seongwon@edu.hanbat.ac.kr" 

WORKDIR /usr/share/nginx/html

COPY ./pages /usr/local/apache2/htdocs/
COPY ./httpd.conf /usr/local/apache2/conf/httpd.conf

EXPOSE 80